package com.example.test2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements LocationListener, View.OnClickListener {

    private TextView mTextViewResult;
    private TextView texto1;
    private LocationManager locMa;
    private Button getClima, getClimaObre, getClimaStPetersburg;
    private double lat, lon;
    private String actual, presion, minima, maxima, humedad;
    private RequestQueue mQueue;

    private String results;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextViewResult = findViewById(R.id.text_view_result);
        getClima = findViewById(R.id.getClima);
        getClima.setOnClickListener(this);
        getClimaObre = findViewById(R.id.getClimaObre);
        getClimaObre.setOnClickListener(this);
        getClimaStPetersburg = findViewById(R.id.getClimaStPetersburg);
        getClimaStPetersburg.setOnClickListener(this);
        locMa = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        mQueue = Volley.newRequestQueue(this);
        Location location = locMa.getLastKnownLocation(locMa.NETWORK_PROVIDER);
        lat = location.getLatitude();
        lon = location.getLongitude();
        onLocationChanged(location);

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void getClima(double lat, double lon){
        mTextViewResult.setText(lat+", "+lon);
        texto1 = (TextView) findViewById(R.id.textView);


        OkHttpClient client = new OkHttpClient();

        String url = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&APPID=6f6dac66a608d477d024d9f0b59c3d59";

        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    final String myResponse = response.body().string();
                    MainActivity.this.runOnUiThread((new Runnable() {
                        @Override
                        public void run() {
                            results = myResponse;
                            try {
                                JSONObject jsonObject = new JSONObject(results);
                                //este String incluye temperatura en Kelvin, presión, humedad, mínima y máxima.
                                mTextViewResult.setText(jsonObject.getString("main"));
                                JSONObject jsonObject1 = new JSONObject(jsonObject.getString("main"));
                                actual = jsonObject1.getString("temp");
                                minima = jsonObject1.getString("temp_min");
                                maxima = jsonObject1.getString("temp_max");
                                humedad = jsonObject1.getString("humidity");
                                presion = jsonObject1.getString("pressure");
                                double temperaturaMax = Double.parseDouble(maxima);
                                if (temperaturaMax >= 305){
                                    texto1.setText("Hoy sera un dia muy caluroso");
                                    Intent intent = new Intent(MainActivity.this, calor.class);
                                    startActivity(intent);


                                }
                                if (temperaturaMax <= 304 && temperaturaMax >= 295 ){
                                    texto1.setText("Hoy sera un dia bastante agradable, tienes mas formas de vestirte ñ.ñ");
                                    Intent intent = new Intent(MainActivity.this, normal.class);
                                    startActivity(intent);

                                }
                                if (temperaturaMax <= 294 ){
                                    texto1.setText("Hoy sera un dia muy fresco");
                                    Intent intent = new Intent(MainActivity.this, frio.class);
                                    startActivity(intent);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==(R.id.getClima)){
            this.getClima(lat, lon);
        }
        if(v.getId()==(R.id.getClimaObre)){
            this.getClima(27.48333,-109.933327);
        }
        if(v.getId()==(R.id.getClimaStPetersburg)){
            this.getClima(59.894444, 30.264168);
        }
    }
}
